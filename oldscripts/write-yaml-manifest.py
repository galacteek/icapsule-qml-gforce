#!/usr/bin/env python

import sys
import os
import argparse
import traceback
from omegaconf import OmegaConf


parser = argparse.ArgumentParser()
parser.add_argument('--template',
                    dest='tmpl',
                    default='manifest.yaml.tmpl')
parser.add_argument('--dst', dest='dest')
parser.add_argument('--cid', dest='cid')
args = parser.parse_args()

if not args.tmpl or not args.cid:
    print('Invalid options')
    sys.exit(1)

os.environ['CID_MAIN'] = args.cid

try:
    manifest = OmegaConf.load(args.tmpl)
    c = OmegaConf.to_container(manifest, resolve=True)
    OmegaConf.save(c, args.dest)
except Exception:
    traceback.print_exc()
    sys.exit(1)


sys.exit(0)
