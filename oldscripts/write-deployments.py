#!/usr/bin/env python

import sys
import uuid
import argparse
import traceback
from datetime import datetime
from omegaconf import OmegaConf


parser = argparse.ArgumentParser()
parser.add_argument('--dst', dest='dest')
parser.add_argument('--manifest-cid', dest='cid')
args = parser.parse_args()

if not args.dest or not args.cid:
    print('Invalid options')
    sys.exit(1)

uid = str(uuid.uuid4())

dpls = {
    'latest': uid,
    'releases': {
        uid: {
            'date': datetime.now().isoformat(),
            'manifestIpfsPath': args.cid
        }
    }
}

try:
    OmegaConf.save(dpls, args.dest)
except Exception:
    traceback.print_exc()
    sys.exit(1)


sys.exit(0)
