import QtQuick 2.8
import QtQuick.Controls 1.2

import GForce.views 1.0

Item {
  visible: true
  width: 640
  height: 480
  property bool debug: false

  Connections {
    target: g_iContainer
    onSizeChanged: {
      width = g_iContainer.getWidth() - 16
      height = g_iContainer.getHeight() - 16

      if (debug) {
        console.debug(`dapp container size changed to ${width}:${height}`)
      }
    }
  }

  Component.onCompleted: {
    console.log('Started embedded dapp')
  }
}
