import QtQuick 2.2

import Galacteek 1.0
import GForce.ipfs 1.0

Item {
  id: upgrade

  property string graphUri
  property string upgradeUri
  property string ttlIpfsPath

  signal finished
  signal failed

  IpfsOperator {
    id: operator1
  }

  RDFGraphOperator {
    id: rdfop
    graphUri: upgrade.graphUri
  }

  Connections {
    target: rdfop
    function onOpDone(rid, result) {
      if (rid === db.ridMerge) {
        if (result === true) {
          rdfop.tAddLiteral(
            upgrade.upgradeUri,
            'ips://galacteek.ld/rdfDatasetMerged',
            '1'
          )

          finished()
        } else if (result === false) {
          console.debug('FAIIIIL')
          failed()
        }
      }
    }
  }

  IpfsObject {
    property string ridMerge
    property alias ttl: upgrade.ttlIpfsPath

    id: db

    function runUpgrade() {
      console.debug('RDF UPGRADE: ' + upgrade.upgradeUri)
      console.debug('IPFSOP READY')

      let res = rdfop.tGetObjFirst(
        upgrade.upgradeUri,
        'ips://galacteek.ld/rdfDatasetMerged'
      )

      if (res === '') {
        console.debug('Doing merge with ' + db.ttl)
        ridMerge = rdfop.ipgRdfMergeFromIpfs(db.ttl)

        console.debug(ridMerge)
      } else {
        upgrade.finished()
      }
    }

    Component.onCompleted: {
      runUpgrade()
    }
  }

  function retry() {
    runUpgrade()
  }
}
