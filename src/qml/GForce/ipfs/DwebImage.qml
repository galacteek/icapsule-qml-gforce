import QtQuick 2.2
import QtQuick.Controls 1.4

Image {
  id: image
  property StackView stack: null

  Component.onCompleted: {
    console.debug('Dweb image loaded from: ' + url)
    console.debug(stack)
  }

  onStatusChanged: if (image.status == Image.Ready) console.log('DwebImg loaded')
  source: g.urlFromGateway(url)

  MouseArea {
    anchors.fill: parent
    onClicked: {
      console.debug(source)

      stack.push({
        item: Qt.resolvedUrl("ILoadResource.qml"),
        properties: { url: source }
      })
    }
  }
}
