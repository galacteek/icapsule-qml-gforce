import QtQuick 2.2
import QtQuick.Controls 1.2
import QtMultimedia 5.0
import QtWebEngine 1.2

Rectangle {
  width: parent.width
  height: parent.height

  property string videoUrl

  Component.onCompleted: {
    console.log('Video player init with URL ' + videoUrl)
  }

  function play() {
    mediaplayer.play()
  }

  MediaPlayer {
    id: mediaplayer
    source: g.urlFromGateway(videoUrl)
  }

  VideoOutput {
    source: mediaplayer
  }

  MouseArea {
    id: playArea
    anchors.fill: parent
    onPressed: {
      mediaplayer.play();
    }
  }
}
