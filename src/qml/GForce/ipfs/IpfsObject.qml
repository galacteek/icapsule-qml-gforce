import QtQuick 2.8
import Galacteek 1.0

Item {
  property bool debug: false
  property bool pin: true
  property string objPath: null
  property string ridPin: null
  property string ridGet: null

  signal ready
  signal fail
  signal fetched

  signal objectOpDone(string rid, var result)

  IpfsOperator {
    id: ipfsop
  }

  Connections {
    target: ipfsop

    function onOpDone(rid, result) {
      if (debug) {
        console.debug('OPDONE: ' + rid + ' with ' + result)
      }
      if (rid === ridPin) {
        if (result === true) {
          ready()
        } else {
          fail()
        }
      } else if (rid === ridGet) {
        if (result === true) {
          fetched()
        }
      } else {
        objectOpDone(rid, result)
      }
    }
  }

  function get() {
    ridGet = ipfsop.get(objPath, {})
  }

  Component.onCompleted: {
    if (pin) {
      ridPin = ipfsop.pin(objPath, {})

      if (debug) {
        console.debug('Pinning: ' + objPath)
        console.debug('RID is: ' + ridPin)
      }
    }
  }
}
