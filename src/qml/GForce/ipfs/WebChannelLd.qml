import QtQuick 2.2
import QtWebEngine 1.2
import QtWebChannel 1.15

QtObject {
    id: channelLd

    // ID, under which this object will be known at WebEngineView side
    WebChannel.id: "ld3"
    property string someProperty: "Break on through to the other side"
    signal someSignal(string message);

    function iObjectUriGen(oclass) {
      return ld.iObjectUriGen(oclass)
    }

    function iObjectCreate(oid, obj) {
      return ld.iObjectCreate(oid, obj)
    }
}
