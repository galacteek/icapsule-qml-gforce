import QtQuick 2.11
import QtQml 2.15

Item {
  property string topic
  property int lifetime: 0
  property bool filterSelf: true
  property bool autoStart: true

  signal message(string sender, variant jsonMsg)
  signal channelStart

  Timer {
    id: timerStart
    interval: 500
    running: true
  
    onTriggered: {
      channelStart()
    }
  } 

  function onJsonMessage(sender, psTopic, psMsg) {
    if (psTopic === topic) {
      message(sender, psMsg)
    }
  }

  function send(psMsg) {
    g_ipfsop.psJsonSend(topic, psMsg)
  }

  function start() {
    console.log('Creating JSON pubsub channel: ' + topic)

    let result = g_ipfsop.psJsonChannelCreate(topic, {
      'lifetime': lifetime,
      'filterSelf': filterSelf
    })
    timerStart.start()
  }

  function stop() {
    g_ipfsop.psJsonChannelDestroy(topic)

    console.debug('Channel destroyed: ' + topic)
  }

  Component.onCompleted: {
    g_ipfsop.psJsonRx.connect(onJsonMessage)

    if (autoStart) {
      start()
    }
  }

  Component.onDestruction: {
    g_ipfsop.psJsonRx.disconnect(onJsonMessage)
    stop()
  }
}
