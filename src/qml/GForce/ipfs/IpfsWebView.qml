import QtQuick 2.2
import QtQuick.Controls 1.2
import QtWebEngine 1.10
import QtWebChannel 1.15

WebEngineView {
  id: webView
  anchors.fill: parent
  zoomFactor: 1.25

  property bool debug: false

  WebChannel {
    id: channel
  }

  // XXX: this is passed from galacteek
  // This is a custom QQuickWebEngineProfile, cloned
  profile: ipfsWebProfile
  webChannel: channel

  backgroundColor: '#323232'
  settings.pluginsEnabled: true
  settings.webGLEnabled: true
  // settings.fullscreenSupportEnabled: true
  // settings.localContentCanAccessRemoteUrls = true

  Component.onCompleted: {
    console.log('IPFS WebEngine: registering channels')

    /*
     * Cannot use registeredObjects (context properties
     * are not available right away it seems) so we register them
     * in the channel here
     *
     */

    channel.registerObjects({
      'g': g,
      'g_ipid': g_ipid,
      'g_ipfsop': g_ipfsop,
      'g_sparql': g_sparql,
      'g_pronto': g_pronto,
      'g_pronto_pairing': g_pronto_pairing
    })
  }

  onNavigationRequested: function(request) {
    console.debug('Navigation request: ' + request.url)
  }

  onNewViewRequested: function(request) {
    console.debug('New view requested: ' + request.requestedUrl)

    // Spawn from the stack
    if (parent.stack) {
      console.debug('Pushing from stack: ' + request.requestedUrl)
      console.debug(parent.stack)
      parent.stack.push({
        item: Qt.resolvedUrl("./ILoadResource.qml"),
        properties: { url: request.requestedUrl, stack: parent.stack }
      })
    } else {
      console.debug('No stack found')
    }
  }

  onLoadingChanged: function(req) {
    if (debug) {
      console.log('IPFS loading changed for: ' + req.url)
    }
  }
}
