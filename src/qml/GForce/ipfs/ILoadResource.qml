import QtQuick 2.2
import QtQuick.Controls 1.2

import GForce.ipfs 1.0

Item {
  property StackView stack: null
  property string url

  Component.onCompleted: {
    console.log('Loading ' + url)
  }

  IpfsWebView {
    anchors.fill: parent
    url: parent.url
  }
}
