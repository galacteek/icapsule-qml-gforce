import QtQuick 2.2
import QtQuick.Controls 2.4
import GForce 1.0

Page {
  default property alias data: inner.data
  anchors.fill: parent

  Item {
    id: inner
    // anchors.centerIn: parent
    // anchors.top: parent.top
    anchors.horizontalCenter: parent.horizontalCenter
  }
}
