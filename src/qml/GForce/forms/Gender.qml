import QtQuick 2.2
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

RowLayout {
  property string label: qsTr("Gender")

  IText {
    text: label
  }

  IComboBox {
    textRole: "name"
    model: ListModel {
      ListElement {
        name: qsTr("Male")
      }
      ListElement {
        name: qsTr("Female")
      }
      ListElement {
        name: qsTr("Undisclosed")
      }
    }
  }
}
