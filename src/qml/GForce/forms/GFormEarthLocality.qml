import QtQuick 2.2
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

import GForce.forms 1.0
import GForce.controls 1.0

RowLayout {
  property alias localityName: picker.currentText

  spacing: 12

  function setLocality(name) {
    picker.displayText = name
  }

  function setCountryUri(uri) {
    picker.setCountryByUri(uri)
  }

  IText {
    text: qsTr("Locality")
  }

  EarthLocalityPicker {
    id: picker
  }
}
