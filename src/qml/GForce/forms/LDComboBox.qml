import QtQuick 2.2
import GForce.models.sparql 1.0


IComboBox {
  property SparQLCoreModel sparqlModel

  id: combo
  enabled: false

  textRole: "name"
  valueRole: "uri"

  signal ready

  Connections {
    target: combo

    onActivated: {
      // var it = combo.model.items.get(index).model
    }
  }

  Connections {
    target: combo.model

    onResultsReady: {
      combo.enabled = true
      combo.ready()
    }

    onBindingsChanged: {
      console.debug('desactivate combo, bindings changed')
      combo.enabled = false
    }
  }
}
