import QtQuick 2.2
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

import GForce.models 1.0


RowLayout {
  property LanguagesModel lModel: LanguagesModel {
    id: lJsonModel
    onReady: {
      // Languages model is ready, preload the Combo box
      combo.preload()
    }
  }

  property string preLang: "English"
  property string label: qsTr("Language")

  function ld() {
    return combo.ldTr()
  }

  IText {
    text: label
  }

  IComboBox {
    id: combo
    textRole: "name"
    model: lJsonModel.model
    font.pixelSize: 20

    function preload() {
      if (preLang != "") {
        let index = lJsonModel.indexForLanguage(preLang)

        if (index >= 0) {
          combo.currentIndex = index
        }
      }
    }

    function ldTr() {
      let item = model.get(currentIndex)

      return {
        "@id": "i:/Language/" + item.name,
        "@type": "Language",
        "name": item.name,
        "alternateName": item.code
      }
    }
  }
}
