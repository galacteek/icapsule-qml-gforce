import QtQuick 2.2
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

ColumnLayout {
  property alias text: textArea.text
  property alias textAreaWidth: textArea.width
  property alias textAreaHeight: textArea.height

  IText {
    text: qsTr("Body (Markdown)")
  }
  ScrollView {
    width: parent.width
    height: parent.height - y

    ITextArea {
      id: textArea
      selectByMouse: true
      font.pixelSize: 24
    }
  }
}
