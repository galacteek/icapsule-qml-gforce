import QtQuick 2.8

Item {
  visible: false

  property var passport
  property bool debug: true

  function id() {
    return passport.me['@id']
  }

  function me() {
    return passport.me
  }

  function address() {
    return passport.me.address
  }

  function homeLocation() {
    return passport.me.homeLocation
  }

  function mainLanguage() {
    return passport.me.mainLanguage
  }

  function secondLanguage() {
    return passport.me.secondLanguage
  }

  function hasNickAndLocation() {
    let nick = me().nickName
    let home = homeLocation()

    if (!nick || !home) {
      return false
    }

    if (home.addressCountry && home.addressLocality) {
      return true
    }
  }

  Component.onCompleted: {
    fetch()
  }

  function fetch() {
    passport = g_ipid.serviceEndpointCompact(
      g_ipid.did(),
      '/passport'
    )

    if (debug) {
      console.debug('Passport dump:')
      console.log(JSON.stringify(passport))
    }
  }
}
