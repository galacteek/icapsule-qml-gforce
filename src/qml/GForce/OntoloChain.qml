import QtQuick 2.2
import Galacteek 1.0

OntologicalSideChain {
  id: chain

  Component.onCompleted: {
    console.debug('Creating chain ' + chainUri)
    chain.create()
  }
}
