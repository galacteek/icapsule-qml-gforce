import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

import QtQuick.Layouts 1.3

TabView {
  anchors.fill: parent
  frameVisible: false

  style: TabViewStyle {
      frameOverlap: 1
      tab: Rectangle {
          color: styleData.selected ? "steelblue" :"lightsteelblue"
          border.color:  "steelblue"
          implicitWidth: Math.max(text.width + 4, 80)
          implicitHeight: 20
          radius: 2
          Text {
              id: text
              anchors.centerIn: parent
              text: styleData.title
              color: styleData.selected ? "white" : "black"
          }
      }

      tabBar: Rectangle {
        Text {
          id: text
          text: styleData.title
          font.pixelSize: 32
        }
      }
      frame: Rectangle { color: "steelblue" }
  }
}
