import QtQuick 2.8
import QtQuick.Controls 1.2
import QtMultimedia 5.15

ListView {
  // property StackView stack
  // property StackView stackView

  id: view
  anchors.fill: parent

  highlight: highlightBar
  highlightFollowsCurrentItem: true

  boundsBehavior: Flickable.StopAtBounds

  SoundEffect {
    id: menuEnterSound
    source: "../sounds/menu-enter.wav"
  }

  delegate: MenuDelegate {
    text: title
    icon: iconSource ? iconSource : "qrc:///share/icons/unknown-file.png"

    onItemEntered: {
      view.currentIndex = index
    }
    onClicked: {
      menuEnterSound.play()
      stackView.pushPage(page, {})
    }
  }

  Component {
   id: highlightBar
   Rectangle {
     width: 400; height: 40
     color: "#4a9ea1"
     y: view.currentItem.y;
     Behavior on y { SpringAnimation { spring: 4; damping: 0.8 } }
    }
  }
}
