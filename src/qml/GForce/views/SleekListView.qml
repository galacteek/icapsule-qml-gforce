import QtQuick 2.8
import QtQuick.Controls 1.2

ListView {
  // keyNavigationEnabled: true
  highlight: highlightBar
  highlightFollowsCurrentItem: true

  boundsBehavior: Flickable.StopAtBounds

  Component {
     id: highlightBar
     Rectangle {
         width: 200; height: 50
         color: "#4a9ea1"
         y: homeView.currentItem.y;
         Behavior on y { SpringAnimation { spring: 2; damping: 0.1 } }
    }
  }
}
