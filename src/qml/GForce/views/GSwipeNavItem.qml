import QtQuick 2.2
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.4

Item {
  default property alias data: inner.data
  id: delegate

  /*
  Button {
    Layout.minimumWidth: 100
    Layout.minimumHeight: 100
    Image {
      anchors.fill: parent
      source: 'qrc:///share/icons/dweb/dweb-arrow-back.png'
      fillMode: Image.PreserveAspectFit
      scale: 3
    }
    // enabled: index > 0
    onClicked: delegate.SwipeView.view.decrementCurrentIndex()
    anchors.left: parent.left
    anchors.leftMargin: 20
    anchors.verticalCenter: parent.verticalCenter
  }
  */

  Image {
    source: 'qrc:///share/icons/dweb/dweb-arrow-back.png'
    fillMode: Image.PreserveAspectFit
    width: 128
    id: back
    visible: index > 0

    MouseArea {
      anchors.fill: parent
      hoverEnabled: true
      onClicked: delegate.SwipeView.view.decrementCurrentIndex()
      onEntered: {
        back.scale = back.scale + 0.5
      }
      onExited: {
        back.scale = back.scale - 0.5
      }
    }

    anchors.left: parent.left
    anchors.leftMargin: 20
    anchors.verticalCenter: parent.verticalCenter
  }



  Item {
    id: inner
    // anchors.centerIn: parent
    anchors.top: parent.top
    anchors.topMargin: 59
    anchors.horizontalCenter: parent.horizontalCenter
  }

  /*
  Button {
    Layout.minimumWidth: 256
    Layout.minimumHeight: 256
    Image {
      anchors.fill: parent
      source: 'qrc:///share/icons/dweb/dweb-arrow-next.png'
      fillMode: Image.PreserveAspectFit
      scale: 3
    }
    // enabled: index < delegate.SwipeView.view.count - 1
    onClicked: delegate.SwipeView.view.incrementCurrentIndex()
    anchors.right: parent.right
    anchors.rightMargin: 20
    anchors.verticalCenter: parent.verticalCenter
  }
  */

  Image {
    source: 'qrc:///share/icons/dweb/dweb-arrow-next.png'
    fillMode: Image.PreserveAspectFit
    width: 128
    id: next
    visible: index < delegate.SwipeView.view.count - 1

    MouseArea {
      anchors.fill: parent
      onClicked: delegate.SwipeView.view.incrementCurrentIndex()
      hoverEnabled: true
      onEntered: {
        next.scale = next.scale + 0.5
      }
      onExited: {
        next.scale = next.scale - 0.5
      }

    }

    anchors.right: parent.right
    anchors.rightMargin: 20
    anchors.verticalCenter: parent.verticalCenter
  }


}
