import QtQuick 2.2

Item {
  id: root
  width: parent.width
  height: 50

  property alias text: textitem.text
  property alias icon: iconitem.source

  signal clicked
  signal itemEntered(int index)
  signal itemReturn(int index)
  property var colors: Array(
    "lightsteelblue",
    "lightskyblue",
    "darkorange",
    "red"
  )

  function randomColor() {
    return colors[Math.floor(Math.random() * colors.length)];
  }

  Rectangle {
    anchors.fill: parent
    color: "#11ffffff"
    visible: mouse.pressed
  }

  Image {
    id: iconitem
    anchors.left: parent.left
    // anchors.rightMargin: 20
    anchors.verticalCenter: parent.verticalCenter
    source: modelData
    fillMode: Image.PreserveAspectFit
    width: 32
  }

  Text {
    id: textitem
    color: "seashell"
    font.pixelSize: 32
    text: modelData
    anchors.verticalCenter: parent.verticalCenter
    anchors.left: parent.left
    anchors.leftMargin: 60
  }

  Rectangle {
    id: itemContent
    anchors.left: parent.left
    anchors.right: parent.right
    anchors.margins: 15
    height: 1
    color: "#424246"
    border.color: 'lightgray'
  }

  Image {
    anchors.right: parent.right
    anchors.rightMargin: 20
    anchors.verticalCenter: parent.verticalCenter
    source: "../images/navigation_next_item.png"
  }

  MouseArea {
    id: mouse
    hoverEnabled: true
    anchors.fill: parent
    onClicked: root.clicked()

    onEntered: {
      itemContent.color = '#4a9ea1'
      itemContent.border.color = randomColor()
      itemContent.border.width = 2

      root.itemEntered(index)
    }
    onExited: {
      itemContent.color = 'lightsteelblue'
      itemContent.border.color = 'lightgray'
      itemContent.border.width = 0
    }
  }
}
