import QtQuick 2.2
import QtQuick.Controls 1.2

Item {
  property StackView stack: null
  property StackView stackView: null

  width: parent.width
  height: parent.height
  anchors.top: parent.top
}
