import QtQuick 2.0
import QtQuick.Window 2.2
import QtLocation 5.6
import QtPositioning 5.6

Rectangle {
  visible: true
  anchors.fill: parent

  Plugin {
    id: mapPlugin
    name: "osm"
  }

  Map {
    id: map
    anchors.fill: parent
    plugin: mapPlugin
    center: QtPositioning.coordinate(59.91, 10.75)
    zoomLevel: 4
  }

  function setCenter(coord) {
    map.center = coord
  }
}
