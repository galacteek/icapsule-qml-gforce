import QtQuick 2.2

JSONListModel {
  id: countriesModel
  source: "countries.json"
  query: "$.*"

  function indexForLanguage(name) {
    // Return the model's index for a given language, or -1 if not found
    var model = countriesModel.model

    for (let idx=0; idx < model.count; idx++) {
      let item = model.get(idx)
      if (item.name === name) {
        return idx
      }
    }
    return -1
  }
}
