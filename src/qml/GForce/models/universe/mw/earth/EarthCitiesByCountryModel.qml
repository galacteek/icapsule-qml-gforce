import GForce.models.sparql 1.0

SparQLCoreModel {
  graphUri: 'urn:ipg:i:uni'
  query: "
    SELECT DISTINCT ?lat ?long ?uri ?name
    WHERE { 
      ?uri a schema:City ;
        geo:lat ?lat ;
        geo:long ?long ;
        dbo:country ?country ;
        foaf:name ?name .
    }
    ORDER BY ?name
  "
}
