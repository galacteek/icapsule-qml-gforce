import GForce.models.sparql 1.0

SparQLCoreModel {
  ident: 'spql-mw-earth-countries'
  cache: true
  cachePrefill: true
  cacheSize: 2048

  graphUri: "urn:ipg:i:uni"
  query: "
    SELECT DISTINCT ?uri ?name
    WHERE { 
      ?uri a schema:Country ;
        foaf:name ?name .
    }
    ORDER BY ?name
  "
}
