import QtQuick 2.8
import Galacteek 1.0

SpQLModel {
  property bool debug: true

  graphUri: 'urn:ipg:i'

  stdPrefixes: {
    'gs': 'ips://galacteek.ld/',
    'schema': 'http://schema.org/',
    'foaf': 'http://xmlns.com/foaf/0.1/',
    'geo': 'http://www.w3.org/2003/01/geo/wgs84_pos#',
    'dbo': 'http://dbpedia.org/ontology/',
    'xsd': 'http://www.w3.org/2001/XMLSchema#',
    'rdf': 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
    'dbpedia': 'http://dbpedia.org/',
    'dc': 'http://purl.org/dc/elements/1.1/'
  }

  signal sparkle

  function run() {
    // clearModel()
    prepare('q0', query)
    runPreparedQuery('q0', bindings)
  }

  Component.onCompleted: {
    run()
  }

  onBindingsChanged: {
    if (debug) {
      console.debug('Bindings changed, rerun ...')
    }

    run()
  }

  onResultsReady: {
    if (debug) {
      console.debug('SparQL results are ready')
    }

    sparkle()
  }
}
