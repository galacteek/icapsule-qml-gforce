SparQLModel {
  debug: true
  prefixes: {'channel': 'ips://galacteek.ld/DwebCommunityChannel#'}
  query: "
    SELECT ?uri ?description ?icon ?title ?dateCreated
    WHERE {
      ?uri a gs:DwebCommunityChannel ;
        gs:title ?title ;
        channel:icon ?icon ;
        gs:description ?description ;
        gs:dateCreated ?dateCreated .
    }
    ORDER BY DESC(?dateCreated)
  "
}
