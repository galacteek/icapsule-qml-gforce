SparQLModel {
  debug: true
  prefixes: {
    'channel': 'ips://galacteek.ld/DwebCommunityChannel#',
    'dwebpost': 'ips://galacteek.ld/DwebSocialPost#'
  }

  query: "
    SELECT ?type ?uri ?body ?dateCreated
    WHERE {
      ?uri a ?type ;
        gs:about ?channelUri ;
        gs:articleBody ?body ;
        gs:dateCreated ?dateCreated .
    }
    ORDER BY DESC(?dateCreated)
  "
}
