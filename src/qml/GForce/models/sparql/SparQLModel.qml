import QtQuick 2.2

ListModel {
  /*
   * ListModel for SparQL results.
   * Queries are executed against galacteek's RDF graphs
   *
   * The model roles match the SparQL variables names
   *
   * Defines custom prefixes via the 'prefixes' property
   * Define your SparQL query with the 'query' variant property
   * Define the query 'bindings' via the 'bindings' variant
   *
   */

  dynamicRoles: true

  property bool cached: true
  property bool processed: false
  property string ident: null // Model identifier
  property bool debug: false
  property string query: ""
  property string graphUri: "urn:ipg:i"
  property variant bindings: null
  property variant prefixes: null
  property int limit: -1
  property variant stdPrefixes: {
    'gs': 'ips://galacteek.ld/',
    'schema': 'http://schema.org/',
    'foaf': 'http://xmlns.com/foaf/0.1/',
    'geo': 'http://www.w3.org/2003/01/geo/wgs84_pos#',
    'dbo': 'http://dbpedia.org/ontology/',
    'xsd': 'http://www.w3.org/2001/XMLSchema#',
    'rdf': 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
    'dbpedia': 'http://dbpedia.org/',
    'dc': 'http://purl.org/dc/elements/1.1/'
  }

  signal sparkle

  function run() {
    clear()

    var fquery = ''

    for (var ns in stdPrefixes) {
       fquery += `PREFIX ${ns}: <` + stdPrefixes[ns] + '>\n'
    }

    for (var ns in prefixes) {
       fquery += `PREFIX ${ns}: <` + prefixes[ns] + '>\n'
    }

    fquery += query

    if (limit > 0) {
      fquery += '\nLIMIT ${limit}'
    }

    if (debug) {
      console.debug('Running query ' + fquery)
    }

    var results = g_sparql.prontoQuery(fquery, graphUri, bindings)

    if (debug) {
      console.debug('Got results ' + JSON.stringify(results))
    }

    results.forEach(function(row) {
      if (debug) {
        console.debug('Got result')
      }
      append(row)
    })

    sparkle()

    processed = true
  }

  onBindingsChanged: {
    run()
  }

  Component.onCompleted: {
    if (!cached || !processed) {
      run()
    } else {
      console.debug('Content is cached')
    }
  }

  function resultsSearch(varname, value) {
    /* Search for a value in the model */
    for (var idx=0; idx < count; idx++) {
      let item = get(idx)

      if (item[varname] === value) {
        return item
      }
    }
  }
}
