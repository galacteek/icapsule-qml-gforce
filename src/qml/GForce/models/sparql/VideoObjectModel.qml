SparQLModel {
  query: "
    SELECT ?url ?dateCreated
    WHERE {
      ?uri a gs:VideoObject ;
        gs:url ?url ;
        gs:dateCreated ?dateCreated .
    }
    LIMIT 1
  "
}
