SparQLCoreModel {
  ident: 'spql-articles'
  // graphUri: 'urn:ipg:i:articles'

  query: "
    SELECT *
    WHERE {
        ?uri a gs:Article ;
          gs:headline ?headline ;
          gs:dateCreated ?dateCreated .
    }
    ORDER BY DESC(?dateCreated)
    LIMIT 100
  "
}
