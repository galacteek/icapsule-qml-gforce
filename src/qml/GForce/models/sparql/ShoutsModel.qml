SparQLCoreModel {
  graphUri: 'urn:ipg:i:articles'
  query: "
    SELECT ?uri ?body ?headline ?dateCreated ?language ?authorNickName
    WHERE {
      ?uri a gs:DwebSocialPost ;
        gs:headline ?headline ;
        gs:articleBody ?body ;
        gs:dateCreated ?dateCreated ;
        gs:author/gs:nickName ?authorNickName ;
        gs:inLanguage/gs:alternateName ?language .
    }
    ORDER BY DESC(?dateCreated)
  "
}
