import QtQuick 2.2
import QtQuick.Controls 2.4

PageIndicator {
  id: indicator

  count: view.count
  currentIndex: view.currentIndex

  anchors.bottom: view.bottom
  anchors.horizontalCenter: parent.horizontalCenter
}
