import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

TabView {
  id: frame
  anchors.fill: parent
  anchors.margins: 4

  style: TabViewStyle {
    tabsAlignment: Qt.AlignHCenter
    frameOverlap: 1
    tabOverlap: 0
    tab: Rectangle {
      color: styleData.selected ? "steelblue" :"lightsteelblue"
      border.color:  "steelblue"
      implicitWidth: Math.max(text.width + 4, 120)
      implicitHeight: 30
      radius: 2
      Text {
        id: text
        anchors.centerIn: parent
        text: styleData.title
        color: styleData.selected ? "white" : "black"
        font.pixelSize: 22
      }
    }
    frame: Rectangle {
      color: "#323232"
    }
  }
}
