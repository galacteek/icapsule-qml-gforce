import QtQuick 2.2
import QtQuick.Controls 2.4
import QtQuick.Controls.Styles 1.1

import GForce.models 1.0

ComboBox {
  font.pixelSize: 20

  property CountriesModel countriesModel: CountriesModel {
    id: countriesModel
  }

  textRole: "name"
  valueRole: "name"

  onActivated: {
    displayText = currentValue
  }

  model: countriesModel.model

  background: Rectangle {
    implicitWidth: 300
    implicitHeight: 42
    opacity: enabled ? 1 : 0.3
  }
}
