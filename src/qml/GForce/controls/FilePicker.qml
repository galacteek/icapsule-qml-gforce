import QtQuick 2.2
import QtQuick.Dialogs 1.0

FileDialog {
  id: fileDialog
  title: qsTr("Please choose a file")
  folder: shortcuts.home

  signal selected(string path)

  onAccepted: {
    var path = fileDialog.fileUrl.toString()
    path = path.replace("file://","")
    selected(path)
  }
  onRejected: {
    console.log("Canceled")
  }
}
