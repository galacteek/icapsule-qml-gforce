import os
import os.path
import re
import sys
import subprocess
import shutil
import codecs
import json
from pathlib import Path

from setuptools import setup
from setuptools import Command
from setuptools import find_packages
from distutils.command.build import build

PY_VER = sys.version_info

if PY_VER >= (3, 6):
    pass
else:
    print('You need python3.6 or newer')
    print('Your python version is {0}'.format(PY_VER))
    raise RuntimeError('Invalid python version')


with open('README.rst', 'r') as fh:
    long_description = fh.read()

deps_links = []


def reqs_parse(path):
    reqs = []
    deps = []

    with open(path) as f:
        lines = f.read().splitlines()
        for line in lines:
            if line.startswith('-e'):
                link = line.split().pop()
                deps.append(link)
            else:
                reqs.append(line)

    return reqs


install_reqs = reqs_parse('requirements.txt')
found_packages = find_packages(exclude=['tests', 'tests.*'])

setup(
    name='icapsule-qml-gforce',
    version='1.0.0',
    license='GPL3',
    author='cipres',
    url='https://gitlab.com/galacteek/icapsule-qml-gforce',
    description='gforce',
    long_description=long_description,
    include_package_data=True,
    packages=found_packages,
    install_requires=install_reqs,
    dependency_links=deps_links,
    package_data={
        '': [
            '*.yaml',
            '*.qss',
            '*.css',
            '*.qrc',
            '*.qml'
        ]
    },
    classifiers=[
        'Environment :: X11 Applications :: Qt',
        'Framework :: AsyncIO',
        'Topic :: Desktop Environment :: File Managers',
        'Topic :: Internet :: WWW/HTTP :: Browsers',
        'Development Status :: 4 - Beta',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Topic :: System :: Filesystems',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9'
    ]
)
